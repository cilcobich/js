var A1=[0, 0], A2=[0, 1], A3=[0, 2];
var B1=[1, 0], B2=[1, 1], B3=[1, 2];
var C1=[2, 0], C2=[2, 1], C3=[2, 2];
var tateti = [];
var current_player = null;
var result = null;
var counter = 0;

 $(document).ready(function() {
    set_init();    
 });

function set_init(){
    $(".cell").html('');
    $("#turn").html('O');
    tateti = [[null,null,null],[null,null,null],[null,null,null]];
    current_player = 10;
    result = null;
    counter = 0;
}

function set_play(id, position){ 
    if($('#' + id ).html() == ''){ 
        if(current_player == 10){
            current_player = 1;
            draw = '୦';
            $("#turn").html('X');
        }else{
            current_player = 10;
            draw = 'ㄨ';
            $("#turn").html('O');
        }

        $('#' + id ).html(draw);
        tateti[position[0]][position[1]] = current_player;
        
        result = check_play();
        if(result != false){
            alert(result);
            set_init();
        }

        counter++;
        if(counter == 9){
            alert('Game Over');
            set_init();
        }
    }
}

function check_play(){
    var combinations = [[A1, A2, A3], [A1, B1, C1], [A1, B2, C3], [B1, B2, B3], [C1, C2, C3], [A3, B3, C3], [A2, B2, C2], [A3, B2, C1]] 
    var combination = null;

    for(c=0; c<combinations.length; c++){
        combination = combinations[c];
        result = tateti[combination[0][0]][combination[0][1]] + tateti[combination[1][0]][combination[1][1]] + tateti[combination[2][0]][combination[2][1]];
        if(result == 3){
            return "Jugador ୦ GANA!";
            break;
        }
        if(result == 30){
            return "Jugador ㄨ GANA!";
            break;
        }
    }
    return false;
}
