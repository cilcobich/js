objects = document.getElementsByTagName("input");
for (var i = 0; i < objects.length; i++ ){
  var ele = objects[i];
  ele.onfocus = delegate(ele, TextOnFocus);
  ele.onblur = delegate(ele, TextOnBlur);
}

function delegate(obj, handler) {
  return function () {
    handler.call(obj);
  }
}

function TextOnFocus(){
    if(this.value == this.getAttribute('placeh')){
        this.value = '';
    }
 }

function TextOnBlur(){
    if(this.value == ''){
        this.value =  this.getAttribute('placeh');
    }
}